$(document).ready(function(){
    $("#tingkat").change(function(){
        $("#dataKonten li").remove();
        if( kelas !== ''){
            $.getJSON( sources + `cekKontenUas}` , function(konten){
                
                if(konten.rows == '0' ){
                    $("#dataKonten").append(`<li class='list-group-item'>Belum Ada Konten</li>`);
                }else{
                    $.each( konten.data , function(i,kntn){
                        $('#dataKonten').append(`
                        <li class='list-group-item'>
                            <p>${kntn.tglPost} - ${kntn.namaMapel} Bab ${kntn.bab}
                            <span class='float-right'> <a class='btn btn-info' id='ktn_${kntn.idKonten}'>Detil</a></span></p>
                        </li>
                        `)
                    })
                }

            })

        }
    })

    $("#jurusan").change(function(){
        $("#datamapel li").remove();
        if( $(this).val() !== ''){
            let kls = $("#tingkat").val(),
                jur = $("#jurusan").val();
            $.getJSON(sources +`lsMapel/${kls}/${jur}` , function(mapels){
                $.each(mapels.data , function(i,data){
                    if(data.niy === niy){
                        $("#datamapel").append(`
                        <li class='list-group-item'>
                        <span class='liNmMapel'>${data.namaMapel}</span><br/>
                        <small class='liNmGuru'>${data.guru}</small>
                        <span class='liKdGuru' style='display:none;'>${data.niy}</span>
                        <span class='liKdMapel' style='display:none;'>${data.kodeMapel}</span>
                        </li>`)
                    }
                    
                })
            })
        }
    })

    $("#datamapel").on('click','.list-group-item',function(){
        let kdMapel , nmMapel, kdGuru, nmGuru, tingkat, jurusan , kdkonten;
        kdMapel = $(this).children('span.liKdMapel').text();
        nmMapel = $(this).children('span.liNmMapel').text();
        kdGuru  = $(this).children('span.liKdGuru').text();
        nmGuru  = $(this).children('small.liNmGuru').text();
        tingkat = $("#tingkat").val();
        jurusan = $("#jurusan").val();
        kdkonten = kdMapel+"-"+tingkat+"-"+jurusan;
        
        $("#fuk_niy").val(kdGuru);
        $("#fuk_kdmp").val(kdMapel);
        $("#fuk_tkt").val(tingkat);

        $("#namaMapel").text(nmMapel);
        $("#namaGuru").text(nmGuru);
        cekKontenTkMp(tingkat,kdMapel);
    })

    $("#dataKonten").on('click','.btn-info',function(){ 
        let idk = $(this).prop('id');
        let nmk = idk.split('_');
        window.location = baseurl + 'Guru/detil/'+nmk[1];
    })
});

function cekKontenTkMp(tk,mp){
    $("#dataKonten li").remove();
        
        
    $.getJSON( sources + `cekKontenTkMp/${tk}/${mp}` , function(konten){
        
        if(konten.rows == '0' ){
            $("#dataKonten").append(`<li class='list-group-item'>Belum Ada Konten</li>`);
        }else{
            $.each( konten.data , function(i,kntn){
                $('#dataKonten').append(`
                <li class='list-group-item'>
                    <p>${kntn.tglPost} - ${kntn.namaMapel} Bab ${kntn.bab}
                    <span class='float-right'> <a class='btn btn-info' id='ktn_${kntn.idKonten}'>Detil</a></span></p>
                </li>
                `)
            })
        }

    })
}