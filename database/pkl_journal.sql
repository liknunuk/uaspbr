DROP TABLE IF EXISTS `pkl_journal`;
CREATE TABLE `pkl_journal` (
  `journalId` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(10) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `gpsinfo` varchar(30) DEFAULT NULL,
  `kegiatan` text DEFAULT NULL,
  `fotoKegiatan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`journalId`),
  KEY `fk_siswa` (`nis`),
  CONSTRAINT `fk_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;