DROP TABLE IF EXISTS siswa;
CREATE TABLE siswa(
  nis varchar(10) NOT NULL,
  nama varchar(40) NOT NULL,
  hp varchar(15) NULL,
  ortu varchar(40) NOT NULL,
  alamat tinytext,
  pkey varchar(4),
  primary key (nis)
);

DROP TABLE IF EXISTS guru;
CREATE TABLE guru(
  niy varchar(10) NOT NULL,
  nama varchar(40) NOT NULL,
  hp varchar(15) NULL,
  primary key(niy)
);

DROP TABLE IF EXISTS kelas;
CREATE TABLE kelas(
  id int(2) NOT NULL AUTO_INCREMENT,
  tingkat enum('X','XI','XII') default 'X',
  jurusan enum('TSM','TKR','TKJ') default 'TSM',
  ruang int(1) default 1,
  kelas varchar(10) NOT NULL,
  primary key(id)
);

DROP TABLE IF EXISTS klsiswa;
CREATE TABLE klsiswa(
  id int(5) NOT NULL AUTO_INCREMENT,
  tapel varchar(9) NOT NULL,
  nis varchar(10) NOT NULL,
  kelas int(2) NOT NULL,
  absen int(2) NOT NULL,
  primary key(id)
);

DROP TABLE IF EXISTS walikls;
CREATE TABLE walikls(
  id int(5) NOT NULL AUTO_INCREMENT,
  tapel varchar(9),
  niy varchar(10),
  kelas int(2),
  primary key(id)
);

DROP TABLE IF EXISTS users;
CREATE TABLE users(
  id int(6) NOT NULL AUTO_INCREMENT,
  uname varchar(20) NOT NULL,
  refftable enum('guru','siswa') default 'siswa',
  password varchar(32),
  primary key(id)
);


DROP TABLE IF  EXISTS sholate;
CREATE TABLE sholate(
  id int(8) NOT NULL AUTO_INCREMENT,
  tapel varchar(9) NOT NULL,
  tanggal date,
  sub enum('0','1') default '0',
  dhu enum('0','1') default '0',
  ash enum('0','1') default '0',
  mag enum('0','1') default '0',
  ish enum('0','1') default '0',
  primary key(id)
);


DROP TABLE IF EXISTS seragame;
CREATE TABLE seragame(
  id int(8) NOT NULL AUTO_INCREMENT,
  tapel varchar(9) NOT NULL,
  nis varchar(8) NOT NULL,
  tanggal date,
  baju enum('0','1') default '0',
  rapi enum('0','1') default '0',
  attr enum('0','1') default '0',
  sock enum('0','1') default '0',
  belt enum('0','1') default '0',
  primary key(id)
);

DROP TABLE IF EXISTS mood;
CREATE TABLE mood(
  id int(8) NOT NULL AUTO_INCREMENT,
  tapel varchar(9) NOT NULL,
  nis varchar(8) NOT NULL,
  tanggal timestamp DEFAULT CURRENT_TIMESTAMP(),
  mood varchar(20),
  primary key(id)
);

DROP TABLE IF EXISTS presensi;
CREATE TABLE presensi(
  id int(8) NOT NULL AUTO_INCREMENT,
  tapel varchar(9) NOT NULL,
  nis varchar(8) NOT NULL,
  tanggal date,
  hadir enum('0','1') DEFAULT '0',
  telat enum('0','1') DEFAULT '0',
  sakit enum('0','1') DEFAULT '0',
  izin  enum('0','1') DEFAULT '0',
  alpha enum('0','1') DEFAULT '0',
  kabur enum('0','1') DEFAULT '0',
  primary key(id)
);


-- views kelas siswa --
CREATE OR REPLACE VIEW vw_klsiswa AS 
SELECT klsiswa.*, siswa.nama , kelas.kelas nmkelas 
FROM klsiswa , siswa , kelas 
WHERE siswa.nis = klsiswa.nis && kelas.id = klsiswa.kelas;
