<?php
class Model_users
{
    private $table = 'users';
    private $db;

    public function __construct(){
        $this->db = new Database();
    }

    public function login($data){
        $password = md5("{$data['usname']}_*_{$data['tasand']}");
        $sql = "SELECT * FROM users WHERE password = :password LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('password',$password);
        $this->db->execute();
        $rows = $this->db->rowCount();
        $data = $this->db->resultOne();
        return array('rows'=>$rows,'data'=>$data);
    }



    public function kredentialSiswa($nis){
        $sql = "SELECT siswa.nama , klsiswa.absen , kelas.kelas   FROM siswa , klsiswa , kelas WHERE siswa.nis = :nis && klsiswa.tapel = :tapel && klsiswa.nis = siswa.nis && kelas.id = klsiswa.kelas";
        $this->db->query($sql);
        $this->db->bind('nis',$nis);
        $this->db->bind('tapel',tahunPelajaran);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function kredentialSiswaUas($nis){
        $sql = "SELECT siswa.nis , siswa.nama , klsiswa.absen , kelas.kelas   FROM siswa , klsiswa , kelas WHERE siswa.nomorus = :nis && klsiswa.tapel = :tapel && klsiswa.nis = siswa.nis && kelas.id = klsiswa.kelas";
        $this->db->query($sql);
        $this->db->bind('nis',$nis);
        $this->db->bind('tapel',tahunPelajaran);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function kredentialTutor($niy){
        $sql = "SELECT nama FROM guru WHERE niy = :niy";
        $this->db->query($sql);
        $this->db->bind('niy',$niy);
        $this->db->execute();
        return $this->db->resultOne();
    }

}