<?php
class Model_guru
{
    private $table = "vw_ListMapel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function uploadKonten($data){
        $sql = "INSERT INTO lms_konten SET niyGuru = :niy , kodeMapel = :kdmp , bab = :bab , tingkat = :tkt , fileKonten = :fk , fileTugas = :ft , fileEvaluasi= :fe , dlTugas = :dlt , dlEvaluasi = :dle";

        $this->db->query($sql);
        $this->db->bind('niy',$data['niy']);
        $this->db->bind('kdmp',$data['kdmp']);
        $this->db->bind('bab',$data['bab']);
        $this->db->bind('tkt',$data['tkt']);
        $this->db->bind('fk',$data['fk']);
        $this->db->bind('ft',$data['ft']);
        $this->db->bind('fe',$data['fe']);
        $this->db->bind('dlt',$data['dlt']);
        $this->db->bind('dle',$data['dle']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function uploadKontenUAS($data){
        $sql = "INSERT INTO uas_konten SET niyGuru = :niy , kodeMapel = :kdmp , fileSoal = :fuas , dnStart = :mldn , dnEnded = :sldn , upStart = :mlup , upEnded = :slup ";

        $this->db->query($sql);
        $this->db->bind('niy',$data['niy']);
        $this->db->bind('kdmp',$data['kdmp']);
        $this->db->bind('fuas',$data['fuas']);
        $this->db->bind('mldn',$data['mldn']);
        $this->db->bind('sldn',$data['sldn']);
        $this->db->bind('mlup',$data['mlup']);
        $this->db->bind('slup',$data['slup']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function lastLmsKontenId(){
        $sql = "SELECT MAX(idKonten) id FROM lms_konten";
        $this->db->query($sql);
        $this->db->execute();
        $res = $this->db->resultOne();
        if($res == NULL){
            $id = '00001';
        }else{
            $id = $res['id'] + 1;
            $id = sprintf('%05d' , $id);
        }
        return $id;
    }

    public function lastUASKontenId(){
        $sql = "SELECT MAX(idKonten) id FROM uas_konten";
        $this->db->query($sql);
        $this->db->execute();
        $res = $this->db->resultOne();
        if($res == NULL){
            $id = '00001';
        }else{
            $id = $res['id'] + 1;
            $id = sprintf('%05d' , $id);
        }
        return $id;
    }

    public function cekKonten($tingkat){
        $sql = "SELECT idKonten , tglPost , lms_konten.kodeMapel , mapel.namaMapel , bab , lms_konten.tingkat FROM lms_konten, mapel WHERE tingkat = :tingkat && mapel.kodeMapel = lms_konten.kodeMapel";

        $this->db->query($sql);
        $this->db->bind('tingkat',$tingkat);
        $this->db->execute();
        $rows = $this->db->rowCount();
        $data = $this->db->resultSet();

        return array('rows'=>$rows , 'data'=>$data);
    }

    public function cekKontenUAS(){
        $sql = "SELECT uas_konten.* , mapel.namaMapel FROM uas_konten, mapel WHERE  mapel.kodeMapel = uas_konten.kodeMapel";

        $this->db->query($sql);
        $this->db->execute();
        $rows = $this->db->rowCount();
        $data = $this->db->resultSet();

        return array('rows'=>$rows , 'data'=>$data);
    }

    public function cekKontenTkMp($tingkat,$kdmp){
        $sql = "SELECT idKonten , tglPost , lms_konten.kodeMapel , mapel.namaMapel , bab , lms_konten.tingkat FROM lms_konten, mapel WHERE tingkat = :tingkat && lms_konten.kodeMapel = :kdmp && mapel.kodeMapel = lms_konten.kodeMapel";

        $this->db->query($sql);
        $this->db->bind('tingkat',$tingkat);
        $this->db->bind('kdmp',$kdmp);
        $this->db->execute();
        $rows = $this->db->rowCount();
        $data = $this->db->resultSet();

        return array('rows'=>$rows , 'data'=>$data);
    }
}