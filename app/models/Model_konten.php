<?php

class Model_konten 
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function last10Konten(){
        $sql = "SELECT idKonten , tglPost , lms_konten.kodeMapel , mapel.namaMapel , bab , lms_konten.tingkat FROM lms_konten, mapel WHERE mapel.kodeMapel = lms_konten.kodeMapel ORDER BY idKonten DESC LIMIT 10";

        $this->db->query($sql);
        $this->db->execute();
        $rows = $this->db->rowCount();
        $data = $this->db->resultSet();

        return array('rows'=>$rows , 'data'=>$data);
    }
    
    public function cekKonten($tingkat){
        $sql = "SELECT idKonten , tglPost , lms_konten.kodeMapel , mapel.namaMapel , bab , lms_konten.tingkat FROM lms_konten, mapel WHERE tingkat = :tingkat && mapel.kodeMapel = lms_konten.kodeMapel";

        $this->db->query($sql);
        $this->db->bind('tingkat',$tingkat);
        $this->db->execute();
        $rows = $this->db->rowCount();
        $data = $this->db->resultSet();

        return array('rows'=>$rows , 'data'=>$data);
    }

    public function cekKontenTkMp($tingkat,$kdmp){
        $sql = "SELECT idKonten , tglPost , lms_konten.kodeMapel , mapel.namaMapel , bab , lms_konten.tingkat FROM lms_konten, mapel WHERE tingkat = :tingkat && lms_konten.kodeMapel = :kdmp && mapel.kodeMapel = lms_konten.kodeMapel";

        $this->db->query($sql);
        $this->db->bind('tingkat',$tingkat);
        $this->db->bind('kdmp',$kdmp);
        $this->db->execute();
        $rows = $this->db->rowCount();
        $data = $this->db->resultSet();

        return array('rows'=>$rows , 'data'=>$data);
    }

    public function dataKonten($idk){
        $sql = "SELECT lms_konten.* , guru.nama , mapel.namaMapel FROM lms_konten , guru, mapel WHERE idKonten = :idk && guru.niy = lms_konten.niyGuru && mapel.kodeMapel = lms_konten.kodeMapel";
        $this->db->query($sql);
        $this->db->bind('idk',$idk);
        $this->db->execute();
        $rows = $this->db->rowCount();
        $data = $this->db->resultOne();
        return array('rows'=>$rows,'data'=>$data);
    }

    public function kontenUas(){
        $sql = "SELECT uas_konten.* , mapel.namaMapel FROM uas_konten,mapel WHERE mapel.kodeMapel = uas_konten.kodeMapel ORDER BY dnStart";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function dataKontenUas($idk){
        $sql = "SELECT uas_konten.* , guru.nama , mapel.namaMapel FROM uas_konten , guru, mapel WHERE idKonten = :idk && guru.niy = uas_konten.niyGuru && mapel.kodeMapel = uas_konten.kodeMapel";
        $this->db->query($sql);
        $this->db->bind('idk',$idk);
        $this->db->execute();
        $rows = $this->db->rowCount();
        $data = $this->db->resultOne();
        return array('rows'=>$rows,'data'=>$data);
    }

    public function getKoleksi($nomor,$tipe){
        $sql = "SELECT k.nis , s.nama , c.kelas , k.fileKoleksi FROM lms_koleksi k , siswa s , klsiswa kl , kelas c WHERE s.nis = k.nis && kl.nis = s.nis && c.id = kl.kelas && k.idKonten = :nomor && k.tipeKoleksi = :tipe ORDER BY kelas, nama";

        $this->db->query($sql);
        $this->db->bind('nomor',$nomor);
        $this->db->bind('tipe',$tipe);
        $this->db->execute();
        return $data = $this->db->resultSet();
    }
}