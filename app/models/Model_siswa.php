<?php
class Model_siswa
{
    private $table = "vw_ListMapel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function uploadKonten($data){
        $sql = "INSERT INTO lms_koleksi SET nis = :nis ,idKonten = :idKonten,tipeKoleksi = :tipeKoleksi ,fileKoleksi = :fileKoleksi";

        $this->db->query($sql);
        $this->db->bind('nis',$data['nis']);
        $this->db->bind('idKonten',$data['idKonten']);
        $this->db->bind('tipeKoleksi',$data['tipeKoleksi']);
        $this->db->bind('fileKoleksi',$data['fileKoleksi']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function uploadKontenUas($data){
        $sql = "INSERT INTO uas_koleksi SET nomorus = :nmus ,fileUas = :fileUas";

        $this->db->query($sql);
        $this->db->bind('nmus',$data['nmus']);
        $this->db->bind('fileUas',$data['fileUas']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function mySubs($nis){
        $sql = "SELECT lms_koleksi.* , mapel.namaMapel FROM lms_koleksi, mapel, lms_konten WHERE mapel.kodeMapel = lms_konten.kodeMapel && lms_koleksi.idKonten = lms_konten.idKonten && lms_koleksi.nis = :nis";

        $this->db->query($sql);
        $this->db->bind('nis',$nis);

        $this->db->execute();
        return $this->db->resultSet();

    }

}