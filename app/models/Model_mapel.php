<?php
class Model_mapel
{
    private $table = "vw_ListMapel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    public function dataMapel($tk,$pd){
        $sql = "SELECT kodeMapel , namaMapel , niyGuru niy , nama guru FROM vw_kontrakMapel WHERE tingkat=:tk && jurusan = :pd  GROUP BY CONCAT(namaMapel,'-',nama) ORDER BY namaMapel";
        $this->db->query($sql);

        // binding data, mengamankan karakter aneh
        $this->db->bind('tk', $tk);
        $this->db->bind('pd', $pd);

        //eksekusi query setelah binding data
        $this->db->execute();

        // pesan baris terpengaruh
        $row = $this->db->rowCount();

        // kembalikan hasil pembacaan data, 1 baris;
        $data = $this->db->resultSet();

        return array('res'=>$row,'data'=>$data);

    }

}
