<?php
class Model_pkl
{
    private $table = "pkl_journal";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function tambahKegiatan($data,$foto){

        //[jid] => [nis] => 18-2-0869 [tgl] => 2019-10-22 [fkg] => [keg] 
        $fotokeg = $this->uploadFoto($foto['fkg']);
        if( $fotokeg == -1 ){
            echo "Ukuran Foto Melebihi 2MB";
            exit();
        }elseif( $fotokeg == 0 ){
            echo "Foto tidak bisa diunggah";
            print_r($foto);
            exit();
        }else{
            $sql = "INSERT INTO " . $this->table . " SET nis = :nis , tanggal = :tgl,  kegiatan = :keg , fotoKegiatan = :fkg";
            $this->db->query($sql);
            $this->db->bind('nis',$data['nis']);
            $this->db->bind('tgl',$data['tgl']);
            $this->db->bind('keg',$data['keg']);
            $this->db->bind('fkg',$foto['fkg']['name']);
            $this->db->execute();
            return $this->db->rowCount();
        }
    }

    private function uploadFoto($foto){
        $fotodir = dirname(__FILE__,3)."/public/albumpkl/";
        if($foto['size'] > 2000000){
            $success = -1;
        }else{
            
            if(move_uploaded_file($foto['tmp_name'], $fotodir . $foto['name'])){
                $success = 1;
            }else{
                $success = 0;
            }
            return $success;
        }
    }

    public function jurnalHariIni($nis){
        $saiki = date('Y-m-d');
        $sql = "SELECT kegiatan, fotoKegiatan FROM pkl_journal WHERE nis = :nis && tanggal = :tgl";
        $this->db->query($sql);
        $this->db->bind('nis',$nis);
        $this->db->bind('tgl',$saiki);
        $this->db->execute();
        return $this->db->resultSet();
    }

}
