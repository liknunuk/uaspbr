<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Login extends Controller
{
  
  // method default
  public function index()
  {
    $data['title'] = "UAS SMKPB Rakit";
            
    $this->view('template/header',$data);
    
    $this->view('home/login',$data);

    $this->view('template/bs4cdn');
    $this->view('template/footer');
  }

  public function auth(){
    $user = $this->model('Model_users')->login($_POST);
    if( $user['rows'] == 0 ){
      $_SESSION['alert'] = "Username Tidak Ditemukan !!";
      header("Location:".BASEURL."Home/Login");
    }

    $udata = $user['data'];

    if($udata['refftable'] == 'siswa'){
      $data = $this->model('Model_users')->kredentialSiswa($_POST['usname']);
      print_r($data);
      $_SESSION['nis'] = $_POST['usname'];
      $_SESSION['nama'] = $data['nama'];
      $_SESSION['absen'] = $data['absen'];
      $_SESSION['kelas'] = $data['kelas'];

      header("Location:".BASEURL."Home");
    }
    elseif($udata['refftable'] == 'guru'){
      $data = $this->model('Model_users')->kredentialTutor($_POST['usname']);
      $_SESSION['nama'] = $data['nama'];
      $_SESSION['niy'] = $_POST['usname'];
      header("Location:".BASEURL."Guru");
    }
  }

  public function logout(){
    session_destroy();
    unset($_SESSION);
    header("Location:".BASEURL."Home/login");
  }
}
