<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Sources extends Controller
{
  // method default
  public function index()
  {
    $this->view('sources/index');
  }

  public function lsMapel($tk,$pd){
      $mapel = $this->model('Model_mapel')->dataMapel($tk,$pd);
      echo json_encode($mapel , JSON_PRETTY_PRINT);
  }

  public function cekKonten($tk){
    $konten = $this->model('Model_guru')->cekKonten($tk);
    echo json_encode($konten);
  }

  public function cekKontenUas(){
    $konten = $this->model('Model_guru')->cekKontenUAS();
    echo json_encode($konten);
  }

  public function cekKontenTkMp($tk,$km){
    $konten = $this->model('Model_guru')->cekKontenTkMp($tk,$km);
    echo json_encode($konten);
  }
}
