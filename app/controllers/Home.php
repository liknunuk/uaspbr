<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  public function __construct(){
    if(!isset($_SESSION['nis'])){
      header("Location:".BASEURL."Login");
      // echo BASEURL;
    }
  }
  // method default
  public function index()
  {
    $data['title'] = "UAS SMKPB Rakit";
    // $data['konten'] = $this->model('Model_konten')->last10Konten();
    $data['konten'] = $this->model('Model_konten')->kontenUas();
    $this->view('template/header',$data);
    $this->view('template/navbar');
    
    $this->view('home/index',$data);

    $this->view('template/bs4cdn');
    $this->view('template/footer');
  }

  public function konten($idk){
    $data['title'] = "LMS SMKPB Rakit";
    
    $data['konten']= $this->model('Model_konten')->dataKonten($idk);
    $data['mySubs']= $this->model('Model_siswa')->mySubs($_SESSION['nis']);
    $this->view('template/header',$data);
    $this->view('template/navbar');
    
    $this->view('home/konten',$data);

    $this->view('template/bs4cdn');
    $this->view('template/footer');
  }

  public function kontenUas($idk){
    $data['title'] = "UAS SMKPB Rakit";
    
    $data['konten']= $this->model('Model_konten')->dataKontenUas($idk);
    $this->view('template/header',$data);
    $this->view('template/navbar');
    
    $this->view('home/kontenUas',$data);

    $this->view('template/bs4cdn');
    $this->view('template/footer');
  }

  public function login(){
      $data['title'] = "LMS SMKPB Rakit";
         
      $this->view('template/header',$data);
      
      $this->view('home/login',$data);
  
      $this->view('template/bs4cdn');
      $this->view('template/footer');
  }

  public function auth(){
    // echo "otentikasi";
    $user = $this->model('Model_users')->login($_POST);
    if( $user['rows'] == 0 ){
      $_SESSION['alert'] = "Username Tidak Ditemukan !!";
      header("Location:".BASEURL."Home/Login");
    }

    $udata = $user['data'];
    // print_r($udata);

    if($udata['refftable'] == 'siswa'){
      $data = $this->model('Model_users')->kredentialSiswaUas($_POST['usname']);
      // print_r($data);
      $_SESSION['nomorus'] = $_POST['usname'];
      $_SESSION['nis'] = $data['nis'];
      $_SESSION['nama'] = $data['nama'];
      $_SESSION['absen'] = $data['absen'];
      $_SESSION['kelas'] = $data['kelas'];

      header("Location:".BASEURL."Home");
    }
    elseif($udata['refftable'] == 'guru'){
      $data = $this->model('Model_users')->kredentialTutor($_POST['usname']);
      $_SESSION['nama'] = $data['nama'];
      $_SESSION['niy'] = $_POST['usname'];
      header("Location:".BASEURL."Guru");
    }
  }

  // upload koleksi tugas/
  public function upTugas(){
        
    $et = $this->getExtension($_FILES['ft']['name']);
    $ftname = 'Tugas_'.$_POST['idp']."-".$_POST['nis'].".".$et;
    $ut = $this->putFile($_FILES['ft'] , $ftname );

    if( $ut == 1 ){
      $data = ['nis'=>$_POST['nis'],'idKonten'=>$_POST['idp'],'tipeKoleksi'=>'tugas','fileKoleksi'=>$ftname];
      if( $this->model('Model_siswa')->uploadKonten($data) > 0){
        header("Location:".BASEURL."Home");
      }
    }
  }

  // upload koleksi evaluasi/
  public function upEval(){
        
    $et = $this->getExtension($_FILES['fe']['name']);
    $ftname = 'Evaluasi_'.$_POST['idp']."-".$_POST['nis'].".".$et;
    $ut = $this->putFile($_FILES['fe'] , $ftname );

    if( $ut == 1 ){
      $data = ['nis'=>$_POST['nis'],'idKonten'=>$_POST['idp'],'tipeKoleksi'=>'evaluasi','fileKoleksi'=>$ftname];
      if( $this->model('Model_siswa')->uploadKonten($data) > 0){
        header("Location:".BASEURL."Home");
      }
    }
  }

  // upload koleksi uas/
  public function upUas(){
    //  print_r($_POST) . print_r($_FILES);
     /* 
     Array ( [mapel] => ASJ [nmuas] => K0308021300534 ) 
     Array ( [berkas] => Array ( [name] => barjas2.xls [type] => application/vnd.ms-excel [tmp_name] => /tmp/phpK0l9TM [error] => 0 [size] => 26624 ) )  */

    $et = $this->getExtension($_FILES['berkas']['name']);
    $ftname = 'UAS_'.date('Y').'_'.$_POST['mapel']."-".$_POST['nmuas'].".".$et;
    $ut = $this->putFile($_FILES['berkas'] , $ftname );

    if( $ut == 1 ){
      $data = ['nmus'=>$_POST['nmuas'],'fileUas'=>$ftname];
      if( $this->model('Model_siswa')->uploadKontenUas($data) > 0){
        header("Location:".BASEURL."Home");
      }
    }
  }

  private function getExtension($ufile){
    return (pathinfo($ufile,PATHINFO_EXTENSION));
  }

  private function putFile($ufile,$fname){
    $targetdir= dirname(__FILE__,3)."/public/pfile/";
    $targetFile = $targetdir.$fname;
    if(move_uploaded_file($ufile['tmp_name'],$targetFile)){
      return 1;
    }
  }

}
