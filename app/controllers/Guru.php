<?php

class Guru extends Controller
{
  public function __construct(){
    if(!isset($_SESSION['niy'])){
      header("Location:".BASEURL."Login");
    }
  }
  // Daftar Mapel yang diampu sebagai halaman induk
  public function index()
  {
    $data['title']  = "LMS Panel Guru";
    $this->view('template/header',$data);
    $this->view('guru/index');
    $this->view('template/footer');
  }

  // upload konten tutorial/
  public function upload(){
    
    $lastId = $this->model('Model_guru')->lastUASKontenId();     
    $ee = $this->getExtension($_FILES['fuk_eva']['name']);
    $efname = 'UAS'.$lastId.".".$ee;
    $ue = $this->putFile($_FILES['fuk_eva'] , $efname);
    if( $ue > 0 ){
      $data = ['niy' =>$_POST['fuk_niy'] , 'kdmp'=>$_POST['fuk_kdmp'] , 'fuas'=>$efname, 'mldn'=>$_POST['fuk_mdn'],'sldn'=>$_POST['fuk_edn'],'mlup'=>$_POST['fuk_mup'],'slup'=>$_POST['fuk_eup']];
      if( $this->model('Model_guru')->uploadKontenUas($data) > 0){
        header("Location:".BASEURL."Guru");
      }
    }
    
  }

  public function detil($id){
    $data['konten'] = $this->model('Model_konten')->dataKonten($id);
    $data['tugas'] = $this->model('Model_konten')->getKoleksi($id,'tugas');
    $data['evaluasi'] = $this->model('Model_konten')->getKoleksi($id,'evaluasi');
    $this->view('template/header',$data);
    $this->view('guru/konten',$data);
    $this->view('template/footer');
  }

  public function detilUas($id){
    $data['title'] = "Detil Dokumen";
    $data['konten'] = $this->model('Model_konten')->dataKontenUas($id);
    $this->view('template/header',$data);
    $this->view('guru/kontenUas',$data);
    $this->view('template/footer');
  }

  private function getExtension($ufile){
    return (pathinfo($ufile,PATHINFO_EXTENSION));
  }

  private function putFile($ufile,$fname){
    $targetdir= dirname(__FILE__,3)."/public/pfile/";
    $targetFile = $targetdir.$fname;
    if(move_uploaded_file($ufile['tmp_name'],$targetFile)){
      return 1;
    }
  }

}
