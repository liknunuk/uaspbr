<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Pkl extends Controller
{
  
  // method default
  public function index()
  {
    $data['title'] = "Jurnal Prakerin";
    $data['jurnal'] = $this->model('Model_pkl')->jurnalHariIni($_SESSION['nis']);
            
    $this->view('template/header',$data);
    
    $this->view('pkl/index',$data);

    $this->view('template/bs4cdn');
    $this->view('template/footer');
  }

  public function save(){
    $savekeg = $this->model('Model_pkl')->tambahKegiatan($_POST,$_FILES);
    if( $savekeg > 0 ){
      header("Location:".BASEURL."/Pkl/");
    }
  }

}
