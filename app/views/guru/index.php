<div class="container-fluid">
    <div class="row mt-3">
        <!-- lift side -->
        <div class="col-lg-3">
            <div class="credensial">
                <h5><?=$_SESSION['nama'];?></h5>
                <h6><?=$_SESSION['niy'];?></h6>
                <a href="<?=BASEURL;?>Login/logout">Logout</a>
            </div>
            <!--form-->
            <div>
                <div class="input-group input-group-sm mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="lms_tingkat">Tingkat</span>
                    </div>
                    <select class="form-control" aria-label="Small" aria-describedby="lms_tingkat" id="tingkat">
                        <option value="">Pilih Kelas</option>
                        <!-- <option value="X">Kelas 10</option>
                        <option value="XI">Kelas 11</option> -->
                        <option value="XII">Kelas 12</option>
                    </select>
                </div>

                <div class="input-group input-group-sm mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="lms_jurusan">Jurusan</span>
                    </div>
                    <select class="form-control" aria-label="Small" aria-describedby="lms_jurusan" id="jurusan">
                        <option value="">Pilih Jurusan</option>
                        <option value="TKJ">Komputer &amp; Jaringan</option>
                        <option value="TKR">Kendaraan Ringan</option>
                    </select>
                </div>
                <!--div class="text-right">
                    <button class="btn btn-primary" id="lms_openmapel">Buka Mapel</button>
                </div-->
            </div>
            <!--form-->
            <ul class="list-group mt-3" id="datamapel"></li>
        </div>
        <!-- lift side -->
        <!-- right side -->
        <div class="col-lg-9">
            <table class="table table-sm bg-dark text-light px-2">
                <tbody>
                    <tr>
                        <td>Form Upload Konten</td>
                        <td id="namaMapel"></td>
                        <td id="namaGuru"></td>
                    </tr>
                </tbody>
            </table>

            <!-- upload konten -->
            <form action="<?=BASEURL;?>Guru/upload" method="post" enctype="multipart/form-data">
            <table class="table table-sm">
                <tbody>
                    <tr>
                        <th>NIY GURU</th>
                        <th>KODE MAPEL</th>
                        <th>TINGKAT</th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" class="form-control" aria-label="_niy" aria-describedby="igk_niy" name="fuk_niy" id="fuk_niy" readonly>
                        </td>
                        <td>
                            <input type="text" class="form-control" aria-label="_kdmp" aria-describedby="igk_kdmp" name="fuk_kdmp" id="fuk_kdmp" readonly>
                        </td>
                        <td>
                            <input type="text" class="form-control" aria-label="_tkt" aria-describedby="igk_tkt" name="fuk_tkt" id="fuk_tkt" readonly>
                        </td>
                    </tr>
                    <tr>
                        <th>BERKAS UAS</th>
                        <th>MULAI DOWNLOAD</th>
                        <th>AKHIR DOWNLOAD</th>
                    </tr>
                    <tr>
                        <td>
                            <input type="file" class="form-control" aria-label="_eva" aria-describedby="igk_eva" name="fuk_eva" id="fuk_eva">
                        </td>
                        <td>
                            <input type="text" name="fuk_mdn" id="fuk_mdn" class="form-control dtp">
                        </td>
                        <td>
                            <input type="text" name="fuk_edn" id="fuk_edn" class="form-control dtp">
                        </td>
                    </tr>
                    <tr>
                        <th>MULAI UPLOAD</th>
                        <th>AKHIR UPLOAD</th>
                        <th>TOMBOL SUBMIT</th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="fuk_mup" id="fuk_mup" class="form-control dtp">
                        </td>
                        <td>
                            <input type="text" name="fuk_eup" id="fuk_eup" class="form-control dtp">
                        </td>
                        <td>
                            <button type="submit" class="btn btn-primary">Unggah Soal</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            </form>
            <!-- upload konten -->
        

            <!-- data konten -->
            <div><h3>DATA KONTEN</h3></div>
            <div class="list-group" id="dataKonten"></div>
            <!-- data konten -->
        </div>
        <!-- right side -->
    </div>
</div>

<?php $this->view('template/bs4cdn');?>
<script>const niy ="<?=$_SESSION['niy'];?>";</script>
<script src="<?=BASEURL;?>js/jquery.datetimepicker.full.js"></script>
<script src="<?=BASEURL;?>js/uas_index.js"></script>
<script>
$('.dtp').datetimepicker();
</script>