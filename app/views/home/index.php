<?php 
    function timestampConvert($tmst){
        list($tanggal,$jam) = explode(" ",$tmst);
        list($tahun,$bulan,$hari) = explode("-",$tanggal);
        return "$hari/$bulan/$tahun $jam";
    }
    
    function cekDownload($mulai,$akhir,$soal){
        $kini = date('Y-m-d H:i:s'); 
        if($kini >= $mulai && $kini <= $akhir ){
            echo "<a href='".BASEURL."pfile/".$soal."' class='btn btn-primary'>Download</a>";
        }else{
            echo "x";
        }
    }

    function cekUpload($mulai,$akhir,$soal){
        $kini = date('Y-m-d H:i:s'); 
        if($kini >= $mulai && $kini <= $akhir ){
            echo "<a href='".BASEURL."Home/kontenUas/{$soal}' class='btn btn-success'>Upload</a>";
        }else{
            echo "x";
        }
    }

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="credensial">
                <h5><?=$_SESSION['nama'];?></h5>
                <h6><?=$_SESSION['nomorus'];?> | <?=$_SESSION['kelas'];?> | <?=$_SESSION['absen'];?></h6>
            </div>
            <div class="text-right credensial px-5" id="jam"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
        <!-- idKonten,tglPost,niyGuru,kodeMapel,fileSoal,dnStart,dnEnded,upStart,upEnded,  -->
            <div class="table-responsive mt-5">
                <div class="row">
                    <div class="col-md-9"><h2>Daftar Unduhan Soal Ujian Sekolah</h2></div>
                    <div class="col-md-3">
                        <a class='btn btn-success' href="<?=BASEURL;?>pfile/LembarJawabUAS.xls">Unduh Lembar Jawab</a>
                    </div>
                </div>
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Mata Pelajaran</th>
                            <th>Mulai Unduh</th>
                            <th>Akhir Unduh</th>
                            <th>Mulai Unggah</th>
                            <th>Akhir Unggah</th>
                            <th>Unduh Berkas</th>
                            <th>Unggah Jawaban</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data['konten'] AS $soal): ?>
                        <tr>
                            <td><?=$soal['namaMapel'];?></td>
                            <td><?=timestampConvert($soal['dnStart']);?></td>
                            <td><?=timestampConvert($soal['dnEnded']);?></td>
                            <td><?=timestampConvert($soal['upStart']);?></td>
                            <td><?=timestampConvert($soal['upEnded']);?></td>
                            <td><?php cekDownload($soal['dnStart'],$soal['dnEnded'],$soal['fileSoal']);?></td>
                            <td><?php cekUpload($soal['upStart'],$soal['upEnded'],$soal['idKonten']);?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table> 
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/bs4cdn'); ?>
<script src="<?=BASEURL;?>js/index.js"></script>
