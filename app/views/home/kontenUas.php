<div class="container">
    <div class="row">
        <div class="col-lg-4">
        <!-- Array ( [nomorus] => K0308021300534 [nis] => 17-1-0682 [nama] => WERI SETIAWAN [absen] => 24 [kelas] => XII-TKR-2 )  -->
            <div class="table-responsive mb-3">
                <table class="table table-sm table-bordered">
                    <tbody>
                        <tr>
                            <th>Nomor Ujian</th><td><?=$_SESSION['nomorus'];?></td>
                        </tr>
                        <tr>
                            <th>N I S</th><td><?=$_SESSION['nis'];?></td>
                        </tr>
                        <tr>
                            <th>Nama</th><td><?=$_SESSION['nama'];?></td>
                        </tr>
                        <tr>
                            <th>Kelas</th><td><?=$_SESSION['kelas'];?></td>
                        </tr>
                        <tr>
                            <th>No. Absen</th><td><?=$_SESSION['absen'];?></td>
                        </tr>
                    </tbody>
                </table>    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <!-- 
            Array ( [idKonten] => 1 [tglPost] => 2020-03-19 14:32:05 [niyGuru] => 00-0002 [kodeMapel] => ASJ [fileSoal] => UAS00001.pdf [dnStart] => 2020-03-19 22:30:00 [dnEnded] => 2020-03-19 23:30:00 [upStart] => 2020-03-19 22:30:00 [upEnded] => 2020-03-19 23:30:00 [nama] => NUGROHO, S.Inf [namaMapel] => ADMINISTRASI SISTEM JARINGAN )
            -->
            <h2>Pengiriman Lembar Jawab</h2>
            <form action="<?=BASEURL;?>Home/upUas" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="lj_mapel" class="col-md-4">Mata Pelajaran</label>
                    <div class="col-md-8"><?=$data['konten']['data']['namaMapel'];?></div>
                    <input type="hidden" name="mapel" value="<?=$data['konten']['data']['kodeMapel'];?>">
                </div>

                <div class="form-group row">
                    <label for="lj_nmuas" class="col-md-4">Nomor Uas</label>
                    <div class="col-md-8">
                        <input type="text" name="nmuas" id="lj_nmuas" class="form-control" value ="<?=$_SESSION['nomorus'];?>" readonly >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lj_berkas" class="col-md-4">Berkas Lembar Jawab</label>
                    <div class="col-md-8">
                        <input type="file" name="berkas" id="lj_berkas" accept=".xls,.xlsx">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <button type="submit" class="float-right btn btn-primary">Unggah Jawaban</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>