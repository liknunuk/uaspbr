<!-- Array ( [nis] => 18-2-0869 [nama] => AJI SETIAWAN [absen] => 4 [kelas] => XI-TKJ-2 ) -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="jumbotron jumbotron-fluid jumbopkl">
                <div class="container">
                    <h1 class="display-4 text-bright">Jurnal Prakerin</h1>
                    <h3 class="lead text-bright">Catatan Harian Kegiatan Praktek Kerja Industri SMK Panca Bhakti Rakit Banjarnegara</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form action="<?=BASEURL;?>Pkl/save" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="jid">ID Jurnal</label>
                    <input type="number" name="jid" id="jpkl_jid" class="form-control" readonly >
                </div>
                <div class="form-group">
                    <label for="nis">Nomor Induk Siswa</label>
                    <input type="text" name="nis" id="jpkl_nis" class="form-control" readonly value="<?=$_SESSION['nis'];?>" >
                </div>
                <div class="form-group">
                    <label for="tgl">Tanggal</label>
                    <input type="date" name="tgl" id="jpkl_tgl" class="form-control" value="<?=date('Y-m-d');?>">
                </div>
                <div class="form-group">
                    <label for="fkg">Foto Kegiatan</label>
                    <input type="file" name="fkg" id="jpkl_fkg" accept="image/png , image/jpg , image/jpeg ">
                </div>
                <div class="form-group">
                    <label for="keg">Uraian Kegiatan</label>
                    <textarea name="keg" id="jpkl_keg" rows="20" class="form-control kegiatan"></textarea>
                </div>
                <div class="form-group">
                    <div class="text-right">
                        <button type="submit" id="jpkl_submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
            <div class="text-right">
                <a href="<?=BASEURL?>" class="btn btn-success">Kembali</a>
            </div>
        </div>
    </div>

    <div class="row">
        <?php $album = BASEURL.'albumpkl/'; ?>
        <?php foreach($data['jurnal'] AS $jurnal): ?>
        <div class="col-lg-4">
            <!-- card -->
            <div class="card" style="width: 18rem;">
                <img src="<?=$album.$jurnal['fotoKegiatan'];?>" class="card-img-top" alt="Kegiatan" width="100%" height="200px">
                <div class="card-body">
                    <p class="card-text"><?=$jurnal['kegiatan'];?></p>
                </div>
            </div>
            <!-- card -->
        </div>
        <?php endforeach; ?>
    </div>
</div>
<!-- skrip tinimce -->
<script type="text/javascript" src="<?=BASEURL;?>js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		tinymce.init({
		selector: ".kegiatan",
		height: 200,
		content_css : "<?=BASEURL;?>css/tinymce.css",
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"
    
	  });

    function sc(){
      console.debug(tinyMCE.activeEditor.getContent());
    }
	</script>
	<!-- skrip tinimce -->